<!DOCTYPE html>
<html>
<head>
	<title>Stocks</title>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/QuoteService.js"></script>

</head>
<body>

	<p id="pElement"></p>

	<script>
	var stock = ["Hello", "Hello2"];
	var pElement = document.getElementById("pElement");
	new Markit.QuoteService("CBSH", function(jsonResult) {
        if (!jsonResult || jsonResult.Message) {
            console.error("Error: ", jsonResult.Message);
            return;
        }
        console.log(jsonResult.Change);
    });

	pElement.innerHTML = stock[0];
	</script>

</body>

</html>